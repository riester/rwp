=== RIESTERWP Core ===
Requires at least: 5.6
Tested up to: 6.0.1
Requires PHP: 7.0
Stable tag: master
License: GPL v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.txt

An internal plugin for websites created by RIESTER to enhance functionality

== Description ==

@TODO - Write Description

== Installation ==

= Uploading in WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Navigate to the 'Upload' area
3. Select `rwp.zip` from your computer
4. Click 'Install Now'
5. Activate the plugin in the Plugin dashboard

= Using FTP =

1. Download `rwp.zip`
2. Extract the `rwp` directory to your computer
3. Upload the `rwp` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard

== Changelog ==

= v0.11.15 - 2022-09-17 =

**Bug Fixes**
* **integrations:** fix active menu item ([6891f89](https://bitbucket.org/riester/rwp/commit/6891f8983b6b65677996392204e45f455f99ebba))
* **internals:** restoring taxonomies ([679f677](https://bitbucket.org/riester/rwp/commit/679f677789bfe75b66db858173197fc16e9fd7a1))


= v0.11.14 - 2022-08-31 =

**Bug Fixes**
* **integrations:** fixes Elementor breakpoints from resetting ([d80063c](https://bitbucket.org/riester/rwp/commit/d80063c7449d0ab3d884b09797145aa0b476f55c))


= v0.11.13 - 2022-08-30 =

**Bug Fixes**
* **ci:** fixes release workflow ([e648e26](https://bitbucket.org/riester/rwp/commit/e648e26b3e9787784eb998069c265d4f984fa7bc))

[See full list of changes here](./CHANGELOG.md)
