# RIESTERWP Core Changelog
---

## [0.11.15](https://bitbucket.org/riester/rwp/compare/v0.11.14...v0.11.15) (2022-09-17)


### Bug Fixes

* **integrations:** fix active menu item ([6891f89](https://bitbucket.org/riester/rwp/commit/6891f8983b6b65677996392204e45f455f99ebba))
* **internals:** restoring taxonomies ([679f677](https://bitbucket.org/riester/rwp/commit/679f677789bfe75b66db858173197fc16e9fd7a1))

## [0.11.14](https://bitbucket.org/riester/rwp/compare/v0.11.13...v0.11.14) (2022-08-31)


### Bug Fixes

* **integrations:** fixes Elementor breakpoints from resetting ([d80063c](https://bitbucket.org/riester/rwp/commit/d80063c7449d0ab3d884b09797145aa0b476f55c))

## [0.11.13](https://bitbucket.org/riester/rwp/compare/v0.11.12...v0.11.13) (2022-08-30)


### Bug Fixes

* **ci:** fixes release workflow ([e648e26](https://bitbucket.org/riester/rwp/commit/e648e26b3e9787784eb998069c265d4f984fa7bc))

## [0.11.12](https://bitbucket.org/riester/rwp/compare/v0.11.11...v0.11.12) (2022-08-30)


### Bug Fixes

* **ci:** fixes release workflow ([99b16ed](https://bitbucket.org/riester/rwp/commit/99b16ed27e7d3270514b1f695a9bb2cc2bbe3cf2))
* **ci:** fixes wp-readme command ([0215c4b](https://bitbucket.org/riester/rwp/commit/0215c4b93aeb6192ab8be9808e16bc903de005f0))

## [0.11.12](https://bitbucket.org/riester/rwp/branches/compare/v0.11.12%0Dv0.11.11) (2022-08-30)


### Bug Fixes

* **ci:** fixes release workflow ([99b16ed](https://bitbucket.org/riester/rwp/commit/99b16ed27e7d3270514b1f695a9bb2cc2bbe3cf2))

## [0.11.11](https://bitbucket.org/riester/rwp/branches/compare/v0.11.11%0Dv0.11.10) (2022-08-30)


### Bug Fixes

* **ci:** fixes release workflow ([42acaf0](https://bitbucket.org/riester/rwp/commit/42acaf0b0bf42c3d805763f61bab0c540310b11d))

## [0.11.10](https://bitbucket.org/riester/rwp/branches/compare/v0.11.10%0Dv0.11.9) (2022-08-30)


### Bug Fixes

* **ci:** fix type of command for publish ([163b56e](https://bitbucket.org/riester/rwp/commit/163b56e4ddfe587a224a3f67ecb1fbad6f027505))
* **ci:** fixes broken packages in pipeline ([7abb985](https://bitbucket.org/riester/rwp/commit/7abb98588c3bf68227992bd1dc0020ca2b4523a6))
* **ci:** fixes package-lock ([565bed9](https://bitbucket.org/riester/rwp/commit/565bed9f05cde0ab50378dbda497d46173982df4))
* **ci:** fixes release notes ([e93bf0f](https://bitbucket.org/riester/rwp/commit/e93bf0f8a6a6e847319b10fe00e057ae8eb990a3))

## [0.11.9](https://bitbucket.org/riester/rwp/branches/compare/v0.11.9%0Dv0.11.8) (2022-08-30)


### Bug Fixes

* **ci:** fix order of commands ([2dc0613](https://bitbucket.org/riester/rwp/commit/2dc06135248f8e6f70315b23e5f4cceffdef176b))

## [0.11.8](https://bitbucket.org/riester/rwp/branches/compare/v0.11.8%0Dv0.11.7) (2022-08-30)


### Bug Fixes

* **integrations:** remove section content width overwrite ([3ad41b7](https://bitbucket.org/riester/rwp/commit/3ad41b7c845156e7c39bc933a4f9d3c1fbe7d80b))

## [0.11.7](https://bitbucket.org/riester/rwp/branches/compare/v0.11.7%0Dv0.11.6) (2022-08-01)


### Bug Fixes

* **core:** fix offcanvas nav bug ([d9222c8](https://bitbucket.org/riester/rwp/commit/d9222c8eca84f02176d2e2f64e0128d8929405c3))

## [0.11.6](https://bitbucket.org/riester/rwp/branches/compare/v0.11.6%0Dv0.11.5) (2022-08-01)


### Bug Fixes

* **core:** add class alias for Collection ([86c98a9](https://bitbucket.org/riester/rwp/commit/86c98a97a6091a8e85b56dde7225ec7681f642c8))

## [0.11.5](https://bitbucket.org/riester/rwp/branches/compare/v0.11.5%0Dv0.11.4) (2022-07-31)


### Bug Fixes

* **pipeline:** add missing dependency ([534a563](https://bitbucket.org/riester/rwp/commit/534a563c4060e3765c82eb137e0759ec31a1a89b))

## [0.11.4](https://bitbucket.org/riester/rwp/branches/compare/v0.11.4%0Dv0.11.3) (2022-07-31)


### Bug Fixes

* **elementor:** remove max-width rule from containers ([515e1c5](https://bitbucket.org/riester/rwp/commit/515e1c5b221492a7392a3b6ca4163371faa44bde))
* **pipeline:** enableing git lfs ([66a6b1d](https://bitbucket.org/riester/rwp/commit/66a6b1d9cc726cc2dfdeae87249d1f0440a10f51))
* **pipeline:** fixing pipeline ([b105e48](https://bitbucket.org/riester/rwp/commit/b105e481c6260ba717583d07cdcfb1a280a85a3f))
* **pipeline:** fixing pipeline ([d0899f5](https://bitbucket.org/riester/rwp/commit/d0899f5b3a7e7110bad12e1f3a91a6dd160dd05c))
* **pipeline:** removing lfs.. ([3a8927e](https://bitbucket.org/riester/rwp/commit/3a8927e4174050b164dbb29c84c3dbde5906359b))

## [0.11.3](https://bitbucket.org/riester/rwp/branches/compare/v0.11.3%0Dv0.11.2) (2022-07-30)


### Bug Fixes

* **semantic-release:** testing CI build ([cf4a4f5](https://bitbucket.org/riester/rwp/commit/cf4a4f5bca05a49535c452168466ea83e685f26f))

## [0.11.2](https://bitbucket.org/riester/rwp/branches/compare/v0.11.2%0Dv0.11.1) (2022-07-28)


### Bug Fixes

* **npm:** fix npm push command ([d8310b5](https://bitbucket.org/riester/rwp/commit/d8310b5d82e1514eaa410523ee48af03210e2caa))

## [0.11.1](https://bitbucket.org/riester/rwp/branches/compare/v0.11.1%0Dv0.11.0) (2022-07-28)


### Bug Fixes

* **pipeline:** merging-steps ([7c68422](https://bitbucket.org/riester/rwp/commit/7c6842207b056afbdc422a22dbcd4918ee1a6268))

## [0.11.0](https://bitbucket.org/riester/rwp/branches/compare/v0.11.0%0Dv0.10.4) (2022-07-28)


### Features

* **acf:** add container fields to plugin settings ([6116265](https://bitbucket.org/riester/rwp/commit/61162655db8450b4360e9f9083c1119264c5a2bc)), closes [RWP-8](https://riester.atlassian.net/jira/software/projects/RWP/issues/8)
* **acf:** updating stylesheet for new container fields ([ae1a84b](https://bitbucket.org/riester/rwp/commit/ae1a84b7db5558ecda37d16d026025b83cc072b8)), closes [RWP-8](https://riester.atlassian.net/jira/software/projects/RWP/issues/8)
* **bootstrap:** integrating the container options ([6ba09a9](https://bitbucket.org/riester/rwp/commit/6ba09a90faa71f9302d0705ec567c8e0748fe9bb)), closes [RWP-8](https://riester.atlassian.net/jira/software/projects/RWP/issues/8)
* **elementor:** integrating the container options ([1b800f1](https://bitbucket.org/riester/rwp/commit/1b800f12b23c1306f290abfaf081f044a094ea0a)), closes [RWP-8](https://riester.atlassian.net/jira/software/projects/RWP/issues/8)


### Bug Fixes

* **elementor:** add missing scss partial ([eb1971c](https://bitbucket.org/riester/rwp/commit/eb1971c805e8f858227825141d48e86dbb23ebea)), closes [RWP-7](https://riester.atlassian.net/jira/software/projects/RWP/issues/7)
* **elementor:** fix Gravity Forms stylesheet enqueue ([b4b7e63](https://bitbucket.org/riester/rwp/commit/b4b7e63a08d36dbb92624d4bac55a04ddfb74517)), closes [RWP-8](https://riester.atlassian.net/jira/software/projects/RWP/issues/8)
* **elementor:** fixing how Elementor widgets are called ([e5c075e](https://bitbucket.org/riester/rwp/commit/e5c075e6e52350aed529533943e8aa79c1099a3a)), closes [RWP-8](https://riester.atlassian.net/jira/software/projects/RWP/issues/8)
* **elementor:** fixing the action to set default features ([c009ebf](https://bitbucket.org/riester/rwp/commit/c009ebf261510a87509250a35e4f009d262d7a90)), closes [RWP-8](https://riester.atlassian.net/jira/software/projects/RWP/issues/8)
* fix Elementor v2 grid ([79b4b8d](https://bitbucket.org/riester/rwp/commit/79b4b8df4ce5e3ea3611f5c0c2927041f382d847)), closes [RWP-7](https://riester.atlassian.net/jira/software/projects/RWP/issues/7) [/riester.atlassian.net/browse/RWP-7](https://riester.atlassian.net/jira/software/projects/RWP/issues/7)
* fix fatal errors ([490b59d](https://bitbucket.org/riester/rwp/commit/490b59d8f4ac34bd46cc6756cf7b6e9b1b25f29f))
* fix issue with class loading in plugin update checker ([4f57764](https://bitbucket.org/riester/rwp/commit/4f5776485f955019b8aeaa4f82b4db7feea4afa2))


### Performance Improvements

* **core:** change how the plugin checks for updates ([f4e9c23](https://bitbucket.org/riester/rwp/commit/f4e9c23d8417b6b85b59e5a877b7081fd0cd6f18))


### Reverts

* Revert "chore(release): 0.10.1 [skip ci]" ([481e31a](https://bitbucket.org/riester/rwp/commit/481e31af3f5967dfdeb8541abe17c8318474a117))
* Revert "revert: "chore(release): 0.10.1 [skip ci]"" ([8996773](https://bitbucket.org/riester/rwp/commit/8996773e50b21684f7c089ca0316a02480da7d9d))
* Revert "ci: remove hard-coded repo url" ([01b9d47](https://bitbucket.org/riester/rwp/commit/01b9d477fdd9a4a7ad522b30e1e546e9a64f9f75))
* Revert "chore(release): 0.10.2 [skip ci]" ([244beb4](https://bitbucket.org/riester/rwp/commit/244beb4e20ecaa56258bdec201a3e1f8042b0ed0))
* revert "chore(release): 0.10.2 [skip ci]" ([ef38bb8](https://bitbucket.org/riester/rwp/commit/ef38bb8b377a74809a2836182d68379409b24973))
* "chore(release): 0.10.1 [skip ci]" ([b97212e](https://bitbucket.org/riester/rwp/commit/b97212ecd237594d50fe7604933a8e15473baea8))

## [0.10.4](https://bitbucket.org/riester/rwp/branches/compare/v0.10.4%0Dv0.10.3) (2022-07-23)


### Bug Fixes

* fix issue with class loading in plugin update checker ([5a20cab](https://bitbucket.org/riester/rwp/commit/5a20cab755c9715686fbd2e75b7a96e3e1f8d3ee))

## [0.10.3](https://bitbucket.org/riester/rwp/branches/compare/v0.10.3%0Dv0.10.2) (2022-07-23)


### Bug Fixes

* fix fatal errors ([079a6d2](https://bitbucket.org/riester/rwp/commit/079a6d2e433c320982a19fe5c5f454d0637e60d6))

## [0.10.2](https://bitbucket.org/riester/rwp/branches/compare/v0.10.2%0Dv0.10.1) (2022-07-23)


### Performance Improvements

* change how the plugin checks for updates\n\nadding YahnisElsts/wp-update-server to digital.riester for plugin distributionInitial commit on laptop ([88390e7](https://bitbucket.org/riester/rwp/commit/88390e7d96f62c582832044ea1476ff62f3f6ed1))

## [0.10.1](https://bitbucket.org/riester/rwp/branches/compare/v0.10.1%0Dv0.10.0) (2022-07-23)


### Bug Fixes

* **acf:** fixing broken file paths for config files ([39e2841](https://bitbucket.org/riester/rwp/commit/39e2841062fff1bb906a8e4df284b076522799e0))
* **bootstrap:** fixing tinyMCE settings ([86fca38](https://bitbucket.org/riester/rwp/commit/86fca381119aa4a2a416cc885944618113081b10))


### Reverts

* Revert "chore(release): 0.10.1 [skip ci]" ([8c40eb3](https://bitbucket.org/riester/rwp/commit/8c40eb31ed1c7f63358216f88501e5fccef1ea08))
* Revert "ci: remove hard-coded repo url" ([02c7e00](https://bitbucket.org/riester/rwp/commit/02c7e009f78e01d3bdbefadd38d4da72390852df))
* Revert "chore(release): 0.10.2 [skip ci]" ([c7007d0](https://bitbucket.org/riester/rwp/commit/c7007d0b7bcc43736934457661509c5c906c6d93))
* revert "chore(release): 0.10.2 [skip ci]" ([e906bd0](https://bitbucket.org/riester/rwp/commit/e906bd05189783d7d854e0d92b40a77fad67d853))

## [0.10.0](https://bitbucket.org/riester/rwp/branches/compare/v0.10.0%0Dv0.9.3) (2022-07-22)


### Features

* FAQ Accordion ([279b0ff](https://bitbucket.org/riester/rwp/commit/279b0ff4833fd652e04285b6c759612a7bd41b68))
* Offcanvas Navigation ([e8e6733](https://bitbucket.org/riester/rwp/commit/e8e6733cd0ec1a4da37ee690ef68ba8184e1c78b))
* **rwp-app.css:** CSS variable for columns widths ([4452a9a](https://bitbucket.org/riester/rwp/commit/4452a9ad27862aa67981a9149c5b8609035dc466))


### Bug Fixes

* Button Icons ([53f6541](https://bitbucket.org/riester/rwp/commit/53f6541316f45f9e1ed1f18fa47f22c64880868b))
* Filled Element function ([5a749b5](https://bitbucket.org/riester/rwp/commit/5a749b5da5ba8ee19c5203ac813e545474a0bf99))
* Fixing missing file reference ([94cce35](https://bitbucket.org/riester/rwp/commit/94cce35bf5eec161ef9da535468fd1898492ac61))
* Object Merging ([9591077](https://bitbucket.org/riester/rwp/commit/9591077cef4f375d219ce63f9f2998000c90f4e7))

## [0.9.3](https://bitbucket.org/riester/rwp/branches/compare/v0.9.3%0Dv0.9.2) (2022-07-12)

## [0.9.2](https://bitbucket.org/riester/rwp/branches/compare/v0.9.2%0Dv0.9.1) (2022-07-12)

## [0.9.1](https://bitbucket.org/riester/rwp/branches/compare/v0.9.1%0Dv0.9.0) (2022-07-11)

## [0.9.0](https://bitbucket.org/riester/rwp/branches/compare/v0.9.0%0Dv0.1.0) (2022-07-11)


### Reverts

* Revert "Refactoring plugin engine" ([5bf8caa](https://bitbucket.org/riester/rwp/commit/5bf8caa2ca04c6e0ae1f1ea65b9db64ed8774000))
